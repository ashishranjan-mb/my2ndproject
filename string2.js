function breakIP(string) {
    let arr = [];
    let newString = string.split(".");
    for(let i of newString) {
        arr.push(parseInt(i))
    }
    if(NaN in arr) {
        return [];
    }
    else {
        return arr;
    }
}
module.exports = breakIP;
