function fullNameTitle(data){
    if (typeof data === 'object' && data != ''){
        let name = ''
        for (let i in data){
            let str = data[i].toLowerCase();
            if (str === "first_name"){
                return "Not Valid Object"
            }
            let value = str[0].toUpperCase();
            let result = value + str.substring(1)
            name = name + " " + result
        }
        if (name.trim() === ''){
            return "Not Valid Object"
        }
        return name.trim()
    }
    else{
        return "Not Valid Object"
    }   
}

module.exports = fullNameTitle;
